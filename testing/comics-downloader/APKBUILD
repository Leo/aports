# Contributor: Anjandev Momi <anjan@momi.ca>
# Maintainer: Anjandev Momi <anjan@momi.ca>
pkgname=comics-downloader
pkgver=0.33.5
pkgrel=0
pkgdesc="Tool to download comics and manga in pdf/epub/cbr/cbz from a website"
url="https://github.com/Girbons/comics-downloader"
license="MIT"
arch="all"
# tests download stuff from random website and compare to predefined values
options="chmod-clean !check"
makedepends="go mesa-dev libxcursor-dev libxrandr-dev libxinerama-dev libxi-dev"
subpackages="$pkgname-gui:gui"
source="$pkgver-$pkgname.tar.gz::https://github.com/Girbons/comics-downloader/archive/refs/tags/v$pkgver.tar.gz"

export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	go build -o build/comics-downloader ./cmd/downloader
	go build -o build/comics-downloader-gui ./cmd/gui
}

check() {
	go test ./...
}

package() {
	install -Dm755 "$builddir"/build/comics-downloader \
		"$pkgdir"/usr/bin/comics-downloader-linux
}

gui() {
	install -Dm755 "$builddir"/build/comics-downloader-gui \
		"$subpkgdir"/usr/bin/comics-downloader-linux-gui
}

sha512sums="
b9ea1cda201476e3a38674d641938f6a1076eb27e3fde7dd1e8f7355f254ad84deca4711c8876033952dfa4e175ba183f255e3296b50c262d2b010e5819d7859  0.33.5-comics-downloader.tar.gz
"
