# Maintainer: Marian Buschsieweke <marian.buschsieweke@ovgu.de>
# Contributor: Marian Buschsieweke <marian.buschsieweke@ovgu.de>
pkgname=kicad-library
pkgver=7.0.5
pkgrel=0
pkgdesc="Kicad component and footprint libraries"
url="https://kicad.github.io/"
# limited by kicad
arch="noarch !armv7 !armhf !riscv64 !s390x"
license="GPL-3.0-or-later"
makedepends="cmake samurai"
depends="kicad"
subpackages="$pkgname-3d:three_d"
source="
	https://gitlab.com/kicad/libraries/kicad-symbols/-/archive/$pkgver/kicad-symbols-$pkgver.tar.gz
	https://gitlab.com/kicad/libraries/kicad-footprints/-/archive/$pkgver/kicad-footprints-$pkgver.tar.gz
	https://gitlab.com/kicad/libraries/kicad-packages3D/-/archive/$pkgver/kicad-packages3D-$pkgver.tar.gz
	"
options="!check" # package only provides data files, so not tests possible

build() {
	cd "$srcdir"/kicad-symbols-$pkgver
	cmake -B build -G Ninja \
		-DCMAKE_INSTALL_PREFIX=/usr
	cmake --build build

	cd "$srcdir"/kicad-footprints-$pkgver
	cmake -B build -G Ninja \
		-DCMAKE_INSTALL_PREFIX=/usr
	cmake --build build

	cd "$srcdir"/kicad-packages3D-$pkgver
	cmake -B build -G Ninja \
		-DCMAKE_INSTALL_PREFIX=/usr
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install "$srcdir"/kicad-symbols-$pkgver/build
	DESTDIR="$pkgdir" cmake --install "$srcdir"/kicad-footprints-$pkgver/build
}

three_d() {
	DESTDIR="$subpkgdir" cmake --install "$srcdir"/kicad-packages3D-$pkgver/build

	# Remove .step version of 3D models; only .wrl versions are needed
	find "$subpkgdir" -name '*.step' -exec rm {} \;
}

sha512sums="
3aeb77672c7f990256a5e2df355029824364c812158c57863b4a37b91521d1cfc73f30f29d873f401b957b788efb15b86bee60695c100ff87723588221b43237  kicad-symbols-7.0.5.tar.gz
be8c5dc717c0575aed55e0794583d0c1fcac71a32b3b62a597e5cc00a3aeefc2aa405a43142f04c98aa070a2ebdeb23df9d50a8a0c1d97f56f7195d494db0ed4  kicad-footprints-7.0.5.tar.gz
c93d3a3309e4d348524e4c2f6e54157edf6922a3940a4e56775cb2eeae81ed5a61569190ebda4c1e990625d3a733a94d62a4200558867277fd6669567f173732  kicad-packages3D-7.0.5.tar.gz
"
