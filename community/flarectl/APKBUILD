# Contributor: Patrycja Rosa <alpine@ptrcnull.me>
# Maintainer: Patrycja Rosa <alpine@ptrcnull.me>
pkgname=flarectl
pkgver=0.70.0
pkgrel=0
pkgdesc="CLI application for interacting with a Cloudflare account"
url="https://github.com/cloudflare/cloudflare-go/tree/master/cmd/flarectl"
arch="all"
license="BSD-3-Clause"
makedepends="go"
source="https://github.com/cloudflare/cloudflare-go/archive/refs/tags/v$pkgver/flarectl-$pkgver.tar.gz"
builddir="$srcdir/cloudflare-go-$pkgver/cmd/flarectl"
options="!check chmod-clean net" # no testsuite provided

export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"
export GOFLAGS="$GOFLAGS -buildvcs=false"

build() {
	go build -o flarectl
}

package() {
	install -Dm755 flarectl -t "$pkgdir"/usr/bin
}

sha512sums="
2a0568536cbf2e74ccf134914b179a99cb1524108c3b3f5cb4769ed564fd05afd1b468c8c9da3e4066881fe95b94d20f0e8d70af18e4198f78e265cd756cc724  flarectl-0.70.0.tar.gz
"
