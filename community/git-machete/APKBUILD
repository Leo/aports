# Contributor: Kevin Daudt <kdaudt@alpinelinux.org>
# Maintainer: Kevin Daudt <kdaudt@alpinelinux.org>
pkgname=git-machete
pkgver=3.17.5
pkgrel=1
pkgdesc="git repository organizer & rebase/merge workflow automation tool"
url="https://github.com/VirtusLab/git-machete"
arch="noarch"
license="MIT"
depends="python3"
makedepends="py3-setuptools"
checkdepends="py3-pytest py3-pytest-mock"
subpackages="
	$pkgname-pyc
	$pkgname-bash-completion
	$pkgname-zsh-completion
	$pkgname-fish-completion
	"
source="https://github.com/VirtusLab/git-machete/archive/refs/tags/v$pkgver/git-machete-$pkgver.tar.gz
	"

build() {
	python3 setup.py build
}

check() {
	pytest
}

package() {
	python3 setup.py install --skip-build --root="$pkgdir"

	install -Dm0644 "$builddir"/completion/git-machete.completion.bash \
		"$pkgdir/usr/share/bash-completion/completions/git-machete"
	install -Dm0644 "$builddir"/completion/git-machete.completion.zsh \
		"$pkgdir/usr/share/zsh/site-functions/_git-machete"
	install -Dm0644 "$builddir"/completion/git-machete.fish \
		-t "$pkgdir/usr/share/fish/vendor_completions.d"
}

sha512sums="
a36c5a8b8002a064bfddb6e0c12b94bd6d4c81ccb9fa5b622b9e55cc3794f5d7fcf4b2ad5df9aac4f563ca7a5ea8a795dc31e1f62db661cfb1b912e259474044  git-machete-3.17.5.tar.gz
"
