# Contributor: Marian Buschsieweke <marian.buschsieweke@ovgu.de>
# Maintainer: Timo Teräs <timo.teras@iki.fi>
pkgname=raspberrypi-bootloader
# To match Alpine kernel schedule, use master branch commit id rather than older stable tagged releases
# Keep by-the-date release numbering for consistency
_commit=9a3a5f6db80eb8cc18e3d5b265422a23a8bbeddd
pkgver=1.20230616
pkgrel=0
pkgdesc="Bootloader files for the Raspberry Pi"
url="https://github.com/raspberrypi/rpi-firmware"
arch="armhf armv7 aarch64"
license="custom"
options="!check !strip !tracedeps !spdx"
source="$pkgname-$pkgver.tar.gz::https://github.com/raspberrypi/rpi-firmware/archive/$_commit.tar.gz"
subpackages="$pkgname-common $pkgname-experimental $pkgname-debug $pkgname-cutdown $pkgname-doc"
depends="$pkgname-common=$pkgver-r$pkgrel"

builddir="$srcdir/rpi-firmware-$_commit"

package() {
	local fw; for fw in bootcode.bin fixup.dat fixup4.dat start.elf start4.elf; do
		install -D "$builddir"/$fw \
			"$pkgdir"/boot/$fw
	done
	install -Dm 644 "$builddir"/LICENCE.broadcom \
		"$pkgdir"/usr/share/licenses/$pkgname/COPYING
}

common() {
	pkgdesc="Common files used by Raspberry Pi bootloaders"
	depends=
	amove boot/bootcode.bin
}

experimental() {
	pkgdesc="Experimental firmware with additional codecs"
	depends="$pkgname-common=$pkgver-r$pkgrel"
	local fw; for fw in start_x.elf start4x.elf fixup_x.dat fixup4x.dat; do
		install -D "$builddir"/$fw \
				"$subpkgdir"/boot/$fw
	done
}

debug() {
	pkgdesc="Debug firmware"
	depends="$pkgname-common=$pkgver-r$pkgrel"
	local fw; for fw in start_db.elf start4db.elf fixup_db.dat fixup4db.dat; do
		install -D "$builddir"/$fw \
			"$subpkgdir"/boot/$fw
	done
}

cutdown() {
	pkgdesc="Cut-down firmware for lower memory settings"
	depends="$pkgname-common=$pkgver-r$pkgrel"
	local fw; for fw in start_cd.elf start4cd.elf fixup_cd.dat fixup4cd.dat; do
		install -D "$builddir"/$fw \
			"$subpkgdir"/boot/$fw
	done
}

sha512sums="
d2df50a98d44ed63bbcf234e8d4071bd194826c9a35bac03fc5d4607df7dbfe3e18e626062a8e17b8caecb212d9a2dca5e800de6d58f5424476f7c9f4ce29516  raspberrypi-bootloader-1.20230616.tar.gz
"
